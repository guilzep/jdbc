package atividade;

public class Carro{
    
    //  ATRIBUTOS DO CARRO:
    private double largura,
            peso,
            altura; // MODIFICADOR PRIVADO
    private String cor; // PRIVADO
    public String nome; // PUBLICO
    
    //  MÉTODO CONSTRUTOR COM 4 ARGUMENTOS
    public Carro(String nome, String cor, double largura, double peso, double altura){
         this.nome = nome;
         this.cor = cor;
         this.largura = largura;
         this.peso = peso;
         this.altura = altura;
    }

    public double getLargura() {
        return largura;
    }

    public void setLargura(double largura) {
        this.largura = largura;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    //  MÉTODO CONSTRUTOR COM NENHUM ARGUMENTO
    public Carro(){
    
    }

    //  MÉTODO QUE MODIFICA A COR DO CARRO
    public void setCor(String cor){
        this.cor = cor;
    }

    //  MÉTODO QUE RETORNA A COR DO CARRO
    public String getCor(){
        return this.cor;
    }
    
    //  MÉTODO QUE FAZ O CARRO ANDAR
    public void andar(int quilometros){

    }
    
    // MÉTODO MAIN
    public static void main(String[] args){
        Carro  fusca = new Carro();
        new Carro("Corolla", "rosa", 1.50, 10000, 1.2);

    }

}
